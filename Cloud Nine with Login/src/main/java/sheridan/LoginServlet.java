package sheridan;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(
        name = "loginservlet",
        urlPatterns = "/Login"
)

public class LoginServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1831978487671842619L;
	
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    String loginName = req.getParameter("LoginName");

    boolean isValidLogin = LoginValidator.isValidLoginName( loginName );

    if ( isValidLogin ) {
      req.setAttribute("loginName", loginName );
      RequestDispatcher view = req.getRequestDispatcher("welcome.jsp");
      view.forward(req, resp);
    }
    else {
      resp.sendRedirect( "index.html" );
    }
  }	
}
