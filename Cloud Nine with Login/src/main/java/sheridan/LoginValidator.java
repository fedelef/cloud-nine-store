package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		return isValidLoginLength(loginName) && isValidLoginCharacters(loginName);
	}
	
	public static boolean isValidLoginLength(String loginName) {
		return loginName.length() >= 6;
	}
	
	public static boolean isValidLoginCharacters(String loginName) {
		if (!Character.isAlphabetic(loginName.charAt(0))) {
			return false;
		}
		
		for (char c : loginName.toCharArray()) {
			if (!(Character.isAlphabetic(c) || Character.isDigit(c))) {
				return false;
			}
		}
		return true;
	}
}
