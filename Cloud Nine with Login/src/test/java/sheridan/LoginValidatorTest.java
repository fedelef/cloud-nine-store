package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {
	
	
	@Test
	public void testIsValidLoginLengthRegular() {
		String loginName = "TigerWoods";
		assertTrue("Invalid length" , LoginValidator.isValidLoginLength(loginName) );
	}
	
	@Test
	public void testIsValidLoginLengthException() {
		String loginName = "Ed";
		assertFalse("Valid length" , LoginValidator.isValidLoginLength(loginName) );
	}
	
	@Test
	public void testIsValidLoginLengthBoundaryIn() {
		String loginName = "Tiger2";
		assertTrue("Invalid length" , LoginValidator.isValidLoginLength(loginName) );
	}
	
	@Test
	public void testIsValidLoginLengthBoundaryOut() {
		String loginName = "Tiger";
		assertFalse("Valid length" , LoginValidator.isValidLoginLength(loginName) );
	}

	
	@Test
	public void testIsValidLoginCharactersRegular() {
		String loginName = "aBc123";
		assertTrue("Invalid characters" , LoginValidator.isValidLoginCharacters(loginName) );
	}
	
	@Test
	public void testIsValidLoginCharactersException() {
		String loginName = "^&*&!^$ abfd_89";
		assertFalse("Valid characters" , LoginValidator.isValidLoginCharacters(loginName) );
	}
	
	@Test
	public void testIsValidLoginCharactersBoundaryIn() {
		String loginName = "a";
		assertTrue("Invalid characters" , LoginValidator.isValidLoginCharacters(loginName) );
	}
	
	@Test
	public void testIsValidLoginCharactersBoundaryOut() {
		String loginName = "2aaaaaaaaaaa";
		assertFalse("Valid characters" , LoginValidator.isValidLoginCharacters(loginName) );
	}
}
